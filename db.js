const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

// import modela
db.osoblje = sequelize.import(__dirname+'/osoblje.js');
db.rezervacija = sequelize.import(__dirname+'/rezervacija.js');
db.termin = sequelize.import(__dirname+'/termin.js');
db.sala = sequelize.import(__dirname+'/sala.js');

// Relacije izmedju modela

// jedna osoba može imati više rezervacija
db.osoblje.hasMany(db.rezervacija, {as: 'rezervacije', foreignKey:'osoba'}); // kreira se atribut osoba u rezervaciji

// jedna rezervacija ima jedan termin
db.rezervacija.belongsTo(db.termin, {foreignKey: 'termin', unique: true}); // kreira se atribut termin u rezervaciji, koji je unique

// jedna sala može imati više rezervacija
db.sala.hasMany(db.rezervacija, {as: 'rezervacije', foreignKey: 'sala'}); // kreira se atribut sala u rezervaciji

// jednu salu zauzima samo jedna osoba
db.sala.belongsTo(db.osoblje, {foreignKey: 'zaduzenaOsoba', unique: true}); // kreira se atribut zaduzenaOsoba u sali

module.exports=db;