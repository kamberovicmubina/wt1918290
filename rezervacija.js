const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Rezervacija = sequelize.define("Rezervacija",{
    	termin: {type: Sequelize.INTEGER, unique: true} // treba biti unique, svi ostali atributi se dodaju iz relacija
    }, {
    	freezeTableName: true
    })
    return Rezervacija;
};
