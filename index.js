const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const url = require('url');
const db = require('./db.js');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/index.js',function(req,res){
    res.sendFile("/index.js", {root: __dirname});
});
app.get('/',function(req,res){
    res.sendFile("/pocetna.html", {root: __dirname});
});

app.get('/rezervacija.html',function(req,res){
    res.sendFile("/rezervacija.html", {root: __dirname});
});
app.get('/pocetna.html',function(req,res){
    res.sendFile("/pocetna.html", {root: __dirname});
});
app.get('/unos.html',function(req,res){
    res.sendFile("/unos.html", {root: __dirname});
});
app.get('/sale.html',function(req,res){
    res.sendFile("/sale.html", {root: __dirname});
});
app.get('/osoblje.html',function(req,res){
    res.sendFile("/osoblje.html", {root: __dirname});
});
app.get('/test/testOsoblje.html',function(req,res){
    res.sendFile("/test/testOsoblje.html", {root: __dirname});
});
app.get('/test/testSale.html',function(req,res){
    res.sendFile("/test/testSale.html", {root: __dirname});
});
app.get('/test/testZauzeca.html',function(req,res){
    res.sendFile("/test/testZauzeca.html", {root: __dirname});
});
app.get('/css/rezervacija.css',function(req,res){
    res.sendFile("/css/rezervacija.css", {root: __dirname});
});
app.get('/css/pocetna.css',function(req,res){
    res.sendFile("/css/pocetna.css", {root: __dirname});
});
app.get('/css/unos.css',function(req,res){
    res.sendFile("/css/unos.css", {root: __dirname});
});
app.get('/css/sale.css',function(req,res){
    res.sendFile("/css/sale.css", {root: __dirname});
});
app.get('/css/meni.css',function(req,res){
    res.sendFile("/css/meni.css", {root: __dirname});
});
app.get('/css/osoblje.css',function(req,res){
    res.sendFile("/css/osoblje.css", {root: __dirname});
});
app.get('/kalendar.js',function(req,res){
    res.sendFile("/kalendar.js", {root: __dirname});
});
app.get('/rezervacije.js',function(req,res){
    res.sendFile("/rezervacije.js", {root: __dirname});
});
app.get('/pocetna.js',function(req,res){
    res.sendFile("/pocetna.js", {root: __dirname});
});
app.get('/pozivi.js',function(req,res){
    res.sendFile("/pozivi.js", {root: __dirname});
});
app.get('/osobljeInit.js',function(req,res){
    res.sendFile("/osobljeInit.js", {root: __dirname});
});
app.get('/test/testOsoblje.js',function(req,res){
    res.sendFile("/test/testOsoblje.js", {root: __dirname});
});
app.get('/test/testSale.js',function(req,res){
    res.sendFile("/test/testSale.js", {root: __dirname});
});
app.get('/test/testZauzeca.js',function(req,res){
    res.sendFile("/test/testZauzeca.js", {root: __dirname});
});
app.get('/img/etf.png',function(req,res){
    res.sendFile("/img/etf.png", {root: __dirname});
});
app.get('/img/sarajevo.jpg',function(req,res){
    res.sendFile("/img/sarajevo.jpg", {root: __dirname});
});
app.get('/img/gradina.jpg',function(req,res){
    res.sendFile("/img/gradina.jpg", {root: __dirname});
});
app.get('/img/mostar.jpg',function(req,res){
    res.sendFile("/img/mostar.jpg", {root: __dirname});
});
app.get('/img/mostar2.jpg',function(req,res){
    res.sendFile("/img/mostar2.jpg", {root: __dirname});
});
app.get('/img/istanbul.jpg',function(req,res){
    res.sendFile("/img/istanbul.jpg", {root: __dirname});
});
app.get('/img/istanbul2.jpg',function(req,res){
    res.sendFile("/img/istanbul2.jpg", {root: __dirname});
});
app.get('/img/pariz.jpg',function(req,res){
    res.sendFile("/img/pariz.jpg", {root: __dirname});
});
app.get('/slike', function(req, res) {
	let tijelo = req.body;
	fs.readdir ("img", (error, files) => {
		if (error) console.log(error);
		var images = [];
		var poc = req.query.pocetna;
		var kraj = req.query.krajnja;
		var i;
		for (i = parseInt(poc); i < parseInt(kraj); i++) {			
			if (files[i] != undefined) {
				images.push("/img/" + files[i]);
			}
		}			
		res.send(images);		
	});
});
// ruta za vracanje svih zauzeca iz baze
app.get('/zauzeca', function(req,res) {
	var objekatZauzeca = {};
	db.sequelize.query("SELECT termin.dan, termin.semestar, sala.naziv, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, osoblje.ime, osoblje.prezime FROM rezervacija, termin, osoblje, sala WHERE rezervacija.termin = termin.id AND termin.redovni = 1 AND rezervacija.sala = sala.id AND rezervacija.osoba = osoblje.id", { type: db.sequelize.QueryTypes.SELECT})
		.then(periodicnaZauzecaUcitana => {			
			var nizPZ = [];
			for (var i = 0; i < periodicnaZauzecaUcitana.length; i++) {
				var obj = {};
				var predavacSpojen = periodicnaZauzecaUcitana[i]["ime"] + " " + periodicnaZauzecaUcitana[i]["prezime"];
				obj["dan"] = periodicnaZauzecaUcitana[i]["dan"];
				obj["semestar"] = periodicnaZauzecaUcitana[i]["semestar"];
				obj["naziv"] = periodicnaZauzecaUcitana[i]["naziv"];
				obj["pocetak"] = periodicnaZauzecaUcitana[i]["pocetak"];
				obj["kraj"] = periodicnaZauzecaUcitana[i]["kraj"];
				obj["predavac"] = predavacSpojen;
				nizPZ.push(obj);
			}
			objekatZauzeca["periodicna"] = nizPZ;
			db.sequelize.query("SELECT termin.datum, sala.naziv, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, osoblje.ime, osoblje.prezime FROM rezervacija, termin, osoblje, sala WHERE rezervacija.termin = termin.id AND termin.redovni = 0 AND rezervacija.sala = sala.id AND rezervacija.osoba = osoblje.id", { type: db.sequelize.QueryTypes.SELECT})
			.then(vanrednaZauzecaUcitana => {
				var nizVZ = [];
				for (var i = 0; i < vanrednaZauzecaUcitana.length; i++) {
					var obj = {};
					var predavacSpojen = vanrednaZauzecaUcitana[i]["ime"] + " " + vanrednaZauzecaUcitana[i]["prezime"];
					obj["datum"] = vanrednaZauzecaUcitana[i]["datum"];
					obj["naziv"] = vanrednaZauzecaUcitana[i]["naziv"];
					obj["pocetak"] = vanrednaZauzecaUcitana[i]["pocetak"];
					obj["kraj"] = vanrednaZauzecaUcitana[i]["kraj"];
					obj["predavac"] = predavacSpojen;
					nizVZ.push(obj);
				}
				objekatZauzeca["vanredna"] = nizVZ;
				res.send(objekatZauzeca);
			})	
		}
	)
	
});
// ruta za ubacivanje novog zauzeca u bazu
app.post('/rezervisi', function(req, res) {
	let tijelo = req.body;
	var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
	if (!mjeseci.includes(tijelo["mjesec"])) res.send("Greška! Mjesec" + tijelo["mjesec"] + " nije u ispravnom formatu!"); // pokušao se poslati nedozvoljeni mjesec (indeks)
	var mjesecBroj = -1;
	for (var i = 0; i < mjeseci.length; i++) {
		if (mjeseci[i] === tijelo["mjesec"]) {
			mjesecBroj = i;
			break;
		}
	}
	var semestar;
		if (mjesecBroj >= 1 && mjesecBroj <= 5) semestar = "ljetni";
		else if (mjesecBroj >= 9 || mjesecBroj == 0) semestar = "zimski";	
		else {
			// ne može se napraviti periodično zauzeće u ljetnim mjesecima kad nije nastava
			if (tijelo["periodicna"] != 'false')
				res.send("Greška! Nije moguće napraviti periodično zauzeće izvan semestra!");
		}
		var dan = tijelo["dan"];
		let prviDan = (new Date(new Date().getFullYear(), mjesecBroj)).getDay(); // vrati broj od 0(ned) do 6(sub)
		if (prviDan == 0) prviDan = 6;
		else prviDan -= 1;
		if (dan == 1) {// odabrali smo prvi dan u mjesecu za rezervaciju 
			dan = prviDan;
		} else {
			dan = ((prviDan + (dan-1))  % 7); // sada je i dan uvijek u intervalu od 0 do 6
		} 
	if (tijelo["periodicna"] != 'false') { // ubacujemo periodicnu rezervaciju
		// uzimamo podatke o terminu
		var pocetak;
		var satiMinute = tijelo["pocetak"].split(":");
		if (satiMinute[0] <= 9 && satiMinute[0].length === 1) pocetak = "0" + satiMinute[0] + ":" + satiMinute[1];
		else pocetak = tijelo["pocetak"];

		var kraj;
		var satiMinute = tijelo["kraj"].split(":");
		if (satiMinute[0] <= 9 && satiMinute[0].length === 1) pocetak = "0" + satiMinute[0] + ":" + satiMinute[1];
		else kraj = tijelo["kraj"];

		var termin = {redovni: true, dan: dan, semestar: semestar, pocetak: pocetak, kraj: kraj};
				
			// provjera da li rezervacija u terminu već postoji u trazenoj sali
			db.sequelize.query("SELECT rezervacija.termin, termin.redovni, termin.dan, termin.datum, termin.semestar, sala.naziv, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, osoblje.ime, osoblje.prezime FROM rezervacija, termin, osoblje, sala WHERE rezervacija.termin = termin.id AND rezervacija.sala = sala.id AND rezervacija.osoba = osoblje.id", { type: db.sequelize.QueryTypes.SELECT})
				.then(svaZauzeca => {
					var postojiRezervacija = false;
					var periodicnaZauzecaNiz = [];
					for (var i = 0; i < svaZauzeca.length; i++) {
						var predavacSpojen = svaZauzeca[i]["ime"] + " " + svaZauzeca[i]["prezime"];
				    	var obj = {};
				    	if (svaZauzeca[i]["redovni"] != 0) {
				    		obj["dan"] = svaZauzeca[i]["dan"];		
				    		obj["semestar"] = svaZauzeca[i]["semestar"];			    	
					    	obj["naziv"] = svaZauzeca[i]["naziv"];
					    	obj["pocetak"] = svaZauzeca[i]["pocetak"];
					    	obj["kraj"] = svaZauzeca[i]["kraj"];
					    	obj["predavac"] = predavacSpojen;				    	 
				    		if (svaZauzeca[i]["semestar"] === semestar && svaZauzeca[i]["dan"] === dan && svaZauzeca[i]["naziv"] === tijelo["sala"] && ((tijelo["pocetak"] >= svaZauzeca[i]["pocetak"] && tijelo["pocetak"] <= svaZauzeca[i]["kraj"]) || (tijelo["kraj"] >= svaZauzeca[i]["pocetak"] && tijelo["kraj"] <= svaZauzeca[i]["kraj"]) ||
																	(svaZauzeca[i]["pocetak"] >= tijelo["pocetak"] && svaZauzeca[i]["pocetak"] <= tijelo["kraj"]) || (svaZauzeca[i]["kraj"] >= tijelo["pocetak"] && svaZauzeca[i]["kraj"] <= tijelo["kraj"]))) {
				    			postojiRezervacija = true;
				    			res.send("Greška! Termin je već zauzeo/la " + predavacSpojen + ".");
				    		}
				    		if (svaZauzeca[i]["redovni"] != 'false') periodicnaZauzecaNiz.push(obj);
				    	} else {
				    		var datum = String(svaZauzeca[i]["datum"]);
				    		var mjesecVanrednog = parseInt(datum.substring(3, 5)); // uzmemo mjesec iz datuma
				    		mjesecVanrednog--;
				    		var danVanrednog = parseInt(datum.substring(0,3)); // uzimamo dan na koji pada datum

				    		let prviDan = (new Date(new Date().getFullYear(), mjesecBroj)).getDay(); 
							if (prviDan == 0) prviDan = 6;
							else prviDan -= 1;
							if (danVanrednog == 1) { 
								danVanrednog = prviDan;
							} else {
								danVanrednog = ((prviDan + (danVanrednog-1))  % 7); 
							} 
				    		if (danVanrednog === dan && mjesecBroj === mjesecVanrednog && svaZauzeca[i]["naziv"] === tijelo["sala"] && ((tijelo["pocetak"] >= svaZauzeca[i]["pocetak"] && tijelo["pocetak"] <= svaZauzeca[i]["kraj"]) || (tijelo["kraj"] >= svaZauzeca[i]["pocetak"] && tijelo["kraj"] <= svaZauzeca[i]["kraj"]) ||
																	(svaZauzeca[i]["pocetak"] >= tijelo["pocetak"] && svaZauzeca[i]["pocetak"] <= tijelo["kraj"]) || (svaZauzeca[i]["kraj"] >= tijelo["pocetak"] && svaZauzeca[i]["kraj"] <= tijelo["kraj"]))) {
				    			postojiRezervacija = true;
				    			res.send("Greška! Termin je već zauzeo/la " + predavacSpojen + ".");
				    		}
				    	}
					}
			    	if (!postojiRezervacija) { // ubacujemo novu rezervaciju u bazu
			    		var idTermina = 0;
						var idZadnjegTermina;		

						// provjera da li termin već postoji u bazi ili treba kreirati novi
						db.sequelize.query("SELECT id, redovni, dan, semestar, TIME_FORMAT(pocetak, '%H:%i') AS pocetak, TIME_FORMAT(kraj, '%H:%i') AS kraj FROM termin", { type: db.sequelize.QueryTypes.SELECT})
							.then(sviTermini => {
								/*var postojiTermin = false;
								for (var i = 0; i < sviTermini.length; i++) {
									if (sviTermini[i]["dan"] === dan && sviTermini[i]["semestar"] === semestar && sviTermini[i]["pocetak"] == pocetak && sviTermini[i]["kraj"] == kraj) {
										//termin već postoji, sve je ok
										postojiTermin = true;
										idTermina = sviTermini[i]["id"];
									}
								}*/
								idZadnjegTermina = sviTermini[sviTermini.length-1]["id"];
								//if (!postojiTermin) { // kreiramo novi termin
									idTermina = idZadnjegTermina + 1;
									db.termin.create(termin);
								//}
			    		var salaId;
						db.sala.findOne({where:{naziv: tijelo["sala"]}}, { type: db.sequelize.QueryTypes.SELECT}).then(function(salaResult){
							salaId = salaResult["id"];

							var osobaId;
							var imePrezime = tijelo["predavac"].split(" ");
							var imeOsobe = imePrezime[0];
							var prezimeOsobe = imePrezime[1];
							db.osoblje.findOne({where:{ime: imeOsobe, prezime: prezimeOsobe}}, { type: db.sequelize.QueryTypes.SELECT}).then(function(osobaResult){
								osobaId = osobaResult["id"];
						    	var novaRezervacija = {dan: dan, semestar: semestar, naziv: tijelo["sala"], pocetak: pocetak, kraj: kraj, predavac: tijelo["predavac"]};
						    	db.rezervacija.create({termin: idTermina, sala: salaId, osoba: osobaId}).then(function(r){
									// vracamo kao odgovor objekat sa svim rezervacijama
									var objekatOdgovor = {};
									periodicnaZauzecaNiz.push(novaRezervacija);
									objekatOdgovor["periodicna"] = periodicnaZauzecaNiz;
									db.sequelize.query("SELECT termin.datum, sala.naziv, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, osoblje.ime, osoblje.prezime FROM rezervacija, termin, osoblje, sala WHERE rezervacija.termin = termin.id AND termin.redovni = 0 AND rezervacija.sala = sala.id AND rezervacija.osoba = osoblje.id", { type: db.sequelize.QueryTypes.SELECT})
										.then(vanrednaZauzeca => {
											var nizVZ = [];
											for (var i = 0; i < vanrednaZauzeca.length; i++) {
												var obj = {};
												var predavacSpojen = vanrednaZauzeca[i]["ime"] + " " + vanrednaZauzeca[i]["prezime"];
												obj["datum"] = vanrednaZauzeca[i]["datum"];
												obj["naziv"] = vanrednaZauzeca[i]["naziv"];
												obj["pocetak"] = vanrednaZauzeca[i]["pocetak"];
												obj["kraj"] = vanrednaZauzeca[i]["kraj"];
												obj["predavac"] = predavacSpojen;
												nizVZ.push(obj);
											}
											objekatOdgovor["vanredna"] = nizVZ;
											res.send(objekatOdgovor);
										}
									)								
									
								});
							});
						});
						});
					}
			});
	} else { // vanredna rezervacija
		var godina = new Date().getFullYear();
		var dvocifreniDan;
		if (tijelo["dan"] <= 9) dvocifreniDan = "0" + tijelo["dan"].toString();
		else dvocifreniDan = tijelo["dan"];
		var dvocifreniMjesec;
		if (mjesecBroj <= 8) dvocifreniMjesec = "0" + (mjesecBroj + 1).toString(); // ako je mjesecBroj=9, to je ustvari 10. mjesec pa ne treba dodavati nulu
		else dvocifreniMjesec = (mjesecBroj + 1).toString();
		var datum = dvocifreniDan + "." + dvocifreniMjesec + "." + godina; 
		var pocetak;
		var satiMinute = tijelo["pocetak"].split(":");
		if (satiMinute[0] <= 9 && satiMinute[0].length === 1) pocetak = "0" + satiMinute[0] + ":" + satiMinute[1];
		else pocetak = tijelo["pocetak"];
		
		var kraj;
		var satiMinute = tijelo["kraj"].split(":");
		if (satiMinute[0] <= 9 && satiMinute[0].length === 1) pocetak = "0" + satiMinute[0] + ":" + satiMinute[1];
		else kraj = tijelo["kraj"];
			
			// provjera da li rezervacija u terminu već postoji u trazenoj sali
			db.sequelize.query("SELECT rezervacija.termin, termin.dan, termin.semestar, termin.datum, sala.naziv, termin.redovni, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, osoblje.ime, osoblje.prezime FROM rezervacija, termin, osoblje, sala WHERE rezervacija.termin = termin.id AND rezervacija.sala = sala.id AND rezervacija.osoba = osoblje.id", { type: db.sequelize.QueryTypes.SELECT})
				.then(svaZauzeca => { 
					var postojiRezervacija = false;
					var vanrednaZauzecaNiz = [];
					for (var i = 0; i < svaZauzeca.length; i++) {
						var predavacSpojen = svaZauzeca[i]["ime"] + " " + svaZauzeca[i]["prezime"];
				    	var obj = {};
				    	if (!svaZauzeca[i]["redovni"]) {
				    		obj["datum"] = svaZauzeca[i]["datum"];				    	
					    	obj["naziv"] = svaZauzeca[i]["naziv"];
					    	obj["pocetak"] = svaZauzeca[i]["pocetak"];
					    	obj["kraj"] = svaZauzeca[i]["kraj"];
					    	obj["predavac"] = predavacSpojen;					    	 
					    	if (svaZauzeca[i]["datum"] === datum && svaZauzeca[i]["naziv"] === tijelo["sala"] && ((tijelo["pocetak"] >= svaZauzeca[i]["pocetak"] && tijelo["pocetak"] <= svaZauzeca[i]["kraj"]) || (tijelo["kraj"] >= svaZauzeca[i]["pocetak"] && tijelo["kraj"] <= svaZauzeca[i]["kraj"]) ||
																		(svaZauzeca[i]["pocetak"] >= tijelo["pocetak"] && svaZauzeca[i]["pocetak"] <= tijelo["kraj"]) || (svaZauzeca[i]["kraj"] >= tijelo["pocetak"] && svaZauzeca[i]["kraj"] <= tijelo["kraj"]))) {
					    		postojiRezervacija = true;
					    		res.send("Greška! Termin je već zauzeo/la " + predavacSpojen + ".");
					    	}
					    	vanrednaZauzecaNiz.push(obj);
					    } else {
					    	var danPeriodicnog = svaZauzeca[i]["dan"];

					    	if (danPeriodicnog === dan && semestar === svaZauzeca[i]["semestar"] && svaZauzeca[i]["naziv"] === tijelo["sala"] && ((tijelo["pocetak"] >= svaZauzeca[i]["pocetak"] && tijelo["pocetak"] <= svaZauzeca[i]["kraj"]) || (tijelo["kraj"] >= svaZauzeca[i]["pocetak"] && tijelo["kraj"] <= svaZauzeca[i]["kraj"]) ||
																	(svaZauzeca[i]["pocetak"] >= tijelo["pocetak"] && svaZauzeca[i]["pocetak"] <= tijelo["kraj"]) || (svaZauzeca[i]["kraj"] >= tijelo["pocetak"] && svaZauzeca[i]["kraj"] <= tijelo["kraj"]))) {
				    			postojiRezervacija = true;
				    			res.send("Greška! Termin je već zauzeo/la " + predavacSpojen + ".");
				    		}
					    }
					}
				    if (!postojiRezervacija) {
				    	var termin = {redovni: false, datum: datum, pocetak: pocetak, kraj: kraj};
						var idTermina = 0;
						var idZadnjegTermina;

						// provjera da li termin već postoji u bazi ili treba kreirati novi
						db.sequelize.query("SELECT id, redovni, datum, TIME_FORMAT(pocetak, '%H:%i') AS pocetak, TIME_FORMAT(kraj, '%H:%i') AS kraj FROM termin", { type: db.sequelize.QueryTypes.SELECT})
							.then(sviTermini => { 
								/*var postojiTermin = false;
								for (var i = 0; i < sviTermini.length; i++) {
									if (sviTermini[i]["datum"] === datum && sviTermini[i]["pocetak"] == pocetak && sviTermini[i]["kraj"] == kraj) {
										//termin već postoji, sve je ok
										postojiTermin = true;
										idTermina = sviTermini[i]["id"];
									}
								}*/
								idZadnjegTermina = sviTermini[sviTermini.length-1]["id"];
								//if (!postojiTermin) { // kreiramo novi termin
									idTermina = idZadnjegTermina + 1;
									db.termin.create(termin);
								//}
				    		var salaId;
							db.sala.findOne({where:{naziv: tijelo["sala"]}}, { type: db.sequelize.QueryTypes.SELECT}).then(function(salaResult){
							salaId = salaResult["id"];

							var osobaId;
							var imePrezime = tijelo["predavac"].split(" ");
							var imeOsobe = imePrezime[0];
							var prezimeOsobe = imePrezime[1];
							db.osoblje.findOne({where:{ime: imeOsobe, prezime: prezimeOsobe}}, { type: db.sequelize.QueryTypes.SELECT}).then(function(osobaResult){
								osobaId = osobaResult["id"];
						    	var novaRezervacija = {datum: datum, naziv: tijelo["sala"], pocetak: pocetak, kraj: kraj, predavac: tijelo["predavac"]};
						    	db.rezervacija.create({termin: idTermina, sala: salaId, osoba: osobaId}).then(function(r){
									// vracamo kao odgovor objekat sa svim rezervacijama
									var objekatOdgovor = {};
									vanrednaZauzecaNiz.push(novaRezervacija);
									objekatOdgovor["vanredna"] = vanrednaZauzecaNiz;
									db.sequelize.query("SELECT termin.dan, termin.semestar, sala.naziv, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, osoblje.ime, osoblje.prezime FROM rezervacija, termin, osoblje, sala WHERE rezervacija.termin = termin.id AND termin.redovni = 1 AND rezervacija.sala = sala.id AND rezervacija.osoba = osoblje.id", { type: db.sequelize.QueryTypes.SELECT})
										.then(periodicnaZauzeca => {
											var nizPZ = [];
											for (var i = 0; i < periodicnaZauzeca.length; i++) {
												var obj = {};
												var predavacSpojen = periodicnaZauzeca[i]["ime"] + " " + periodicnaZauzeca[i]["prezime"];
												obj["dan"] = periodicnaZauzeca[i]["dan"];
												obj["semestar"] = periodicnaZauzeca[i]["semestar"];
												obj["naziv"] = periodicnaZauzeca[i]["naziv"];
												obj["pocetak"] = periodicnaZauzeca[i]["pocetak"];
												obj["kraj"] = periodicnaZauzeca[i]["kraj"];
												obj["predavac"] = predavacSpojen;
												nizPZ.push(obj);
											}
											objekatOdgovor["periodicna"] = nizPZ;
											res.send(objekatOdgovor);
										}
									)	
								});
						    });
						});
				    
						});
					}
			});
	}


});
app.get('/osoblje', function(req,res) {
	db.osoblje.findAll({
	  attributes: ['id', 'ime', 'prezime', 'uloga']
	}).then(
		osoblje => {
			res.send(osoblje)
		}
	)
});
app.get('/sale', function(req,res) {
	db.sala.findAll({
		attributes: ['naziv', 'zaduzenaOsoba']
	}).then(
		sale => {
			res.send(sale);
		}
	)
});
app.get('/termini/:osobaId', function(req,res) { 
	var osobaId = req.params.osobaId;
	db.sequelize.query("SELECT termin.dan, termin,datum, termin.semestar, TIME_FORMAT(termin.pocetak, '%H:%i') AS pocetak, TIME_FORMAT(termin.kraj, '%H:%i') AS kraj, sala.naziv FROM termin, rezervacija, sala WHERE termin.id = rezervacija.termin AND rezervacija.osoba = '" + osobaId + "' AND sala.id = rezervacija.sala", { type: db.sequelize.QueryTypes.SELECT})
	.then(terminiOsobe =>{
				var imaTrenutnoNastavu = false;
				// poredimo da li se termini koje je zauzela ova osoba preklapaju sa trenutnim vremenom i datumom
				if (terminiOsobe.length === 0) sala = "U kancelariji"; // ako nema nijednu rezervaciju u bazi, u kancelariji je
				var danasnjiDatum = new Date();
				var dd = String(danasnjiDatum.getDate()).padStart(2, '0');
				var mm = String(danasnjiDatum.getMonth() + 1).padStart(2, '0'); 
				var yyyy = danasnjiDatum.getFullYear();
				danasnjiDatum = dd + '.' + mm + '.' + yyyy;

				for (var i = 0; i < terminiOsobe.length; i++){ // prolazimo kroz sve termine koje ima ta osoba rezervisane					
					if (terminiOsobe[i]["datum"] != null) {
						// prvo provjerimo datum						
						if (danasnjiDatum === terminiOsobe[i]["datum"]) {
							// sada provjeravamo vrijeme
							var today = new Date();
							var minute, sati;
							if (today.getMinutes() < 10) minute = "0" + today.getMinutes();
							else minute = today.getMinutes();
							if(today.getHours() < 10) sati = "0" + today.getHours();
							else sati = today.getHours();
							var vrijeme = sati + ":" + minute;
							if (String(terminiOsobe[i]["pocetak"]) <= vrijeme && vrijeme <= String(terminiOsobe[i]["kraj"])) {
								imaTrenutnoNastavu = true;
								sala = terminiOsobe[i]["naziv"];
								console.log(sala);
								res.send(sala);
							}
						}
					} else {
						// pretvaramo redni broj dana u mjesecu da je u intervalu od 0 do 6
						var dan = dd;
						var danMjesec = parseInt(mm);
						danMjesec--;
						let prviDan = (new Date(new Date().getFullYear(), danMjesec)).getDay(); // vrati broj od 0(ned) do 6(sub)
						if (prviDan == 0) prviDan = 6;
						else prviDan -= 1;
						if (dan == 1) {// odabrali smo prvi dan u mjesecu za rezervaciju 
							dan = prviDan;
						} else {
							dan = ((prviDan + (dan-1))  % 7); 
						} 
						
						if ((terminiOsobe[i]["semestar"] === "zimski" && (danMjesec >= 9 || danMjesec == 0) && terminiOsobe[i]["dan"] === dan) || (terminiOsobe[i]["semestar"] === "ljetni" && (danMjesec >= 1 || danMjesec <= 5) && terminiOsobe[i]["dan"] === dan)) {
							// sada provjeravamo vrijeme
							var today = new Date();
							var minute, sati;
							if (today.getMinutes() < 10) minute = "0" + today.getMinutes();
							else minute = today.getMinutes();
							if(today.getHours() < 10) sati = "0" + today.getHours();
							else sati = today.getHours();
							var vrijeme = sati + ":" + minute;
							console.log(vrijeme);
							console.log(osobaId, "Pocetak: ", terminiOsobe[i]["pocetak"]);
							console.log(osobaId, "Kraj: ", terminiOsobe[i]["kraj"]);
							if (String(terminiOsobe[i]["pocetak"]) <= vrijeme && vrijeme <= String(terminiOsobe[i]["kraj"])) {
								imaTrenutnoNastavu = true;
								sala = terminiOsobe[i]["naziv"];
								console.log(sala);
								res.send(sala);
							}
						}
					}
					
				}
				if (imaTrenutnoNastavu == false) { 
						sala = "U kancelariji";
						console.log(sala);
						res.send(sala);
				}
		
	});
});
var server = app.listen(8080);
module.exports = server;

