let Kalendar = (function () {
	var vanredna;
	var periodicna;

	function obojiCrveno(dan) {
		var dani = document.getElementsByClassName("outterDiv");	
		for (var j = 0; j < dani.length; j++) {
			if (parseInt(dani[j].innerHTML) == dan && !dani[j].classList.contains("zauzeta")) {
				dani[j].classList.remove("slobodna");
				dani[j].classList.add("zauzeta");
				//dani[j].onclick = null;
				break;
			} 
		}
	}

	function obojiCrvenoPeriodicno(redniBrojDana) {
		var dijete = document.querySelector("#containerDiv :nth-child(" + redniBrojDana + ")");
		if (dijete != null && dijete.classList.contains("outterDiv") && !dijete.classList.contains("zauzeta")) {
			dijete.classList.remove("slobodna");
			dijete.classList.add("zauzeta");
			//dijete.onclick = null;
		}
	}


	function obojiZeleno () {
		var dani = document.getElementsByClassName("outterDiv");			
		for (var j = 0; j < dani.length; j++) {
			if (dani[j].classList.contains("zauzeta")) {
				dani[j].classList.remove("zauzeta");
				dani[j].classList.add("slobodna");
			}
		}
	}

	function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
		obojiZeleno();
		var i;
		var slobodna = true;
		if (this.vanredna != null) {
			for (i = 0; i < this.vanredna.length; i++) {
				var m = parseInt(this.vanredna[i].datum.slice(3,5));
				var dan = parseInt(this.vanredna[i].datum.slice(0,2));
				if (m == (mjesec+1) && this.vanredna[i].naziv == sala && ((pocetak >= this.vanredna[i].pocetak && pocetak <= this.vanredna[i].kraj) || (kraj >= this.vanredna[i].pocetak && kraj <= this.vanredna[i].kraj) ||
																	(this.vanredna[i].pocetak >= pocetak && this.vanredna[i].pocetak <= kraj) || (this.vanredna[i].kraj >= pocetak && this.vanredna[i].kraj <= kraj)) ) {
					slobodna = false;					
					obojiCrveno(dan);
				}				
			}
		}

		if (this.periodicna != null) {
			for (i = 0; i < this.periodicna.length; i++) {
				var semestar = this.periodicna[i].semestar;
				if (semestar === "zimski") {
					if ((mjesec >= 9 || mjesec == 0) && this.periodicna[i].naziv == sala && ((pocetak >= this.periodicna[i].pocetak && pocetak <= this.periodicna[i].kraj) || (kraj >= this.periodicna[i].pocetak && kraj <= this.periodicna[i].kraj) ||
														(this.periodicna[i].pocetak >= pocetak && this.periodicna[i].pocetak <= kraj) || (this.periodicna[i].kraj >= pocetak && this.periodicna[i].kraj <= kraj)) ) {
						slobodna = false;
						var dan = this.periodicna[i].dan;
						//sada boji sve dane koji u sedmici dodju pod tim rednim brojem npr 0-sve ponedjeljke, 1-sve utorke
						for (var redniBrojDana = parseInt(dan) + 8; redniBrojDana < 44 ; redniBrojDana += 7) {
							obojiCrvenoPeriodicno(redniBrojDana);
						}
					}
		
				} else if ((semestar === "ljetni") && this.periodicna[i].naziv == sala && ((pocetak >= this.periodicna[i].pocetak && pocetak <= this.periodicna[i].kraj) || (kraj >= this.periodicna[i].pocetak && kraj <= this.periodicna[i].kraj) ||
													(this.periodicna[i].pocetak >= pocetak && this.periodicna[i].pocetak <= kraj) || (this.periodicna[i].kraj >= pocetak && this.periodicna[i].kraj <= kraj)) ) {
					if (mjesec >= 1 && mjesec <= 5) {
						slobodna = false;
						var dan = this.periodicna[i].dan;
						for (var redniBrojDana = parseInt(dan) + 8; redniBrojDana < 44 ; redniBrojDana += 7)
							obojiCrvenoPeriodicno(redniBrojDana);
					} 
				} 
			}
			
		}
		if (slobodna) {
			var dani = document.getElementsByClassName("outterDiv");	
			if (dani != null) {		
				for (var j = 0; j < dani.length; j++) {
					if (dani[j].classList.contains("zauzeta"))
						dani[j].classList.remove("zauzeta");
						dani[j].classList.add("slobodna");
				}
			}
		}
		
	}

	function ucitajPodatkeImpl(periodicna, vanredna) {
		this.periodicna = periodicna;
		this.vanredna = vanredna;
		console.log(periodicna);
		console.log(vanredna);
	}

	function iscrtajKalendarImpl(kalendarRef, mjesec) {
		var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
		document.getElementById("trenutni").innerHTML = mjeseci[mjesec];
		if (mjesec == 11) document.getElementById("nextBtn").disabled = true;
		if (mjesec == 0) document.getElementById("prevBtn").disabled = true;

		var brojDana = 32 - new Date(new Date().getFullYear(), mjesec, 32).getDate();
		var date = new Date();
		let prviDan = (new Date(date.getFullYear(), mjesec)).getDay();
		if (prviDan == 0) prviDan = 7; // nedjelja je broj 0, pa onda ispise 6 slobodnih mjesta
		var j;
		for (var j = 1; j < prviDan; j++) {
			var emptyDiv = document.createElement("div");
			kalendarRef.appendChild(emptyDiv);
		}
		var ids = ["prvi", "drugi", "treci", "cetvrti", "peti", "sesti", "sedmi"];
		// kreiranje prve sedmice
		var brojac = 0;
		for (j = prviDan; j <= 7; j++) {
			brojac++;
			var outterNode = document.createElement("div");
			outterNode.classList.add("outterDiv");
			outterNode.classList.add("slobodna");
			outterNode.id = ids[j-1];
			var tekst = document.createTextNode(brojac);
			outterNode.appendChild(tekst);
			outterNode.onclick = function () {
				if (sveIspunjeno()) { 
					if(outterNode.classList.contains("slobodna")) {
						Swal.fire({
							  title: 'Želite li rezervisati ovaj termin?',
							  icon: 'warning',
							  showCancelButton: true,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'DA',
							  cancelButtonText: 'NE'
						}).then((result) => {
							  if (result.value) {
							    var dann = this.firstChild.nodeValue; 
								Pozivi.uputiPostZahtjev(dann, document.getElementById("periodicnaCheck").checked, document.getElementById("trenutni").innerHTML, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value, document.getElementById("osobeSelect").value, (periodicnaParam, vanrednaParam) => {
									console.log("LAmbda funkcija, ovdje su ", periodicnaParam);
									console.log("LAmbda funkcija, ovdje su ", vanrednaParam);
									Kalendar.ucitajPodatke(periodicnaParam, vanrednaParam);
									Kalendar.obojiZauzeca(kalendarRef, mjesec, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value);
								});
							  }
						})
					}
				} else {
					Swal.fire({
					  icon: 'error',
					  title: 'Greška',
					  text: 'Rezervacija nije moguća. Neispravni podaci o rezervaciji!'					  
					})
				}
			};
			var innerNode = document.createElement("div");
			innerNode.classList.add("bottomDiv");
			outterNode.appendChild(innerNode);
			kalendarRef.appendChild(outterNode);

		}

		//ostatak mjeseca
		for (var i = brojac; i < brojDana; i++) {
			brojac++;
			var outterNode = document.createElement("div");
			outterNode.classList.add("outterDiv");
			outterNode.classList.add("slobodna");			
			var tekst = document.createTextNode(brojac);
			outterNode.appendChild(tekst);
			outterNode.onclick = function () {
				if (sveIspunjeno()) { 
					if (outterNode.classList.contains("slobodna")) {
						Swal.fire({
							  title: 'Želite li rezervisati ovaj termin?',
							  icon: 'warning',
							  showCancelButton: true,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'DA',
							  cancelButtonText: 'NE'
						}).then((result) => {
							  if (result.value) {
							    var dann = this.firstChild.nodeValue; 
								Pozivi.uputiPostZahtjev(dann, document.getElementById("periodicnaCheck").checked, document.getElementById("trenutni").innerHTML, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value, document.getElementById("osobeSelect").value, (periodicnaParam, vanrednaParam) => {
									Kalendar.ucitajPodatke(periodicnaParam, vanrednaParam);
									Kalendar.obojiZauzeca(kalendarRef, mjesec, document.getElementById("sala").value, document.getElementById("pocetak").value, document.getElementById("kraj").value);
								});
							  }
						})
					} 					
				} else {
					Swal.fire({
					  icon: 'error',
					  title: 'Greška',
					  text: 'Rezervacija nije moguća. Neispravni podaci o rezervaciji!'					  
					})
				}
			};
			var innerNode = document.createElement("div");
			innerNode.classList.add("bottomDiv");
			outterNode.appendChild(innerNode);
			kalendarRef.appendChild(outterNode);
		}		

	}
	return {
		obojiZauzeca: obojiZauzecaImpl,
		ucitajPodatke: ucitajPodatkeImpl,
		iscrtajKalendar: iscrtajKalendarImpl
	}
}());

function sveIspunjeno () {
	return document.getElementById("sala").value != "--" && document.getElementById("osobeSelect").value != "--" && document.getElementById("pocetak").value != "" && document.getElementById("kraj").value != "";
}

function removeAllChildNodes () {
	const node = document.getElementById("containerDiv");
	while (node.firstChild) {
		node.removeChild(node.firstChild);		
	}
	
	var label = document.createElement("label");
	label.classList.add("pon");
	var tekst = document.createTextNode("PON");
	label.appendChild(tekst);
	node.appendChild(label);

	var label = document.createElement("label");
	label.classList.add("uto");
	var tekst = document.createTextNode("UTO");
	label.appendChild(tekst);
	node.appendChild(label);

	var label = document.createElement("label");
	label.classList.add("sri");
	var tekst = document.createTextNode("SRI");
	label.appendChild(tekst);
	node.appendChild(label);

	var label = document.createElement("label");
	label.classList.add("cet");
	var tekst = document.createTextNode("CET");
	label.appendChild(tekst);
	node.appendChild(label);

	var label = document.createElement("label");
	label.classList.add("pet");
	var tekst = document.createTextNode("PET");
	label.appendChild(tekst);
	node.appendChild(label);

	var label = document.createElement("label");
	label.classList.add("sub");
	var tekst = document.createTextNode("SUB");
	label.appendChild(tekst);
	node.appendChild(label);

	var label = document.createElement("label");
	label.classList.add("ned");
	var tekst = document.createTextNode("NED");
	label.appendChild(tekst);
	node.appendChild(label);
}


function sljedeciMjesec () {
	removeAllChildNodes();
	var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
	var i, brojMjeseca;
	for (i = 0; i < mjeseci.length; i++) {
		if (mjeseci[i] === document.getElementById("trenutni").innerHTML) {
			brojMjeseca = i;
			break;
		}
	}
	if (brojMjeseca == 0) document.getElementById("prevBtn").disabled = false;
	else if (brojMjeseca == 10) document.getElementById("nextBtn").disabled = true;
	if (brojMjeseca != 11) document.getElementById("trenutni").innerHTML = mjeseci[brojMjeseca+1];
	Kalendar.iscrtajKalendar(document.getElementById("containerDiv") , brojMjeseca + 1);
	oboji();
}

function prethodniMjesec () {
	removeAllChildNodes();
	var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
	var i, brojMjeseca;
	for (i = 0; i < mjeseci.length; i++) {
		if (mjeseci[i] === document.getElementById("trenutni").innerHTML) {
			brojMjeseca = i;
			break;
		}
	}
	if (brojMjeseca == 1) document.getElementById("prevBtn").disabled = true;
	else if (brojMjeseca == 11) document.getElementById("nextBtn").disabled = false;
	if (brojMjeseca != 0) document.getElementById("trenutni").innerHTML = mjeseci[brojMjeseca-1];
	Kalendar.iscrtajKalendar(document.getElementById("containerDiv") , brojMjeseca - 1);
	oboji();
}

function oboji () {
	var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
	var i, brojMjeseca;
	for (i = 0; i < mjeseci.length; i++) {
		if (mjeseci[i] === document.getElementById("trenutni").innerHTML) {
			brojMjeseca = i;
			break;
		}
	}
	var nazivSale = document.getElementById("sala").value;
	var vrijeme = document.getElementById("pocetak").value;
	var kraj = document.getElementById("kraj").value;

	if (nazivSale != null)
		Kalendar.obojiZauzeca(document.getElementById("containerDiv"), brojMjeseca, nazivSale, vrijeme, kraj);
	
}

