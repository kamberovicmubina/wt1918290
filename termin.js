const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Termin = sequelize.define("Termin",{
        redovni:Sequelize.BOOLEAN,
        dan : {
        	type: Sequelize.INTEGER,
        	field: 'dan',
        	allowNull: true
        },
        datum: {
        	type: Sequelize.STRING,
        	field: 'datum',
        	allowNull: true
        },
        semestar: {
        	type: Sequelize.STRING,
        	field: 'semestar',
        	allowNull: true
        },
        pocetak:Sequelize.TIME,
        kraj:Sequelize.TIME
    }, {
        freezeTableName: true
    })
    return Termin;
};
