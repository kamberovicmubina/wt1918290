window.onload = function () {
	Pozivi.ucitajPodatke((periodicna, vanredna) => {
		Kalendar.iscrtajKalendar(document.getElementById("containerDiv") , new Date().getMonth());
		Kalendar.ucitajPodatke(periodicna, vanredna);
	});

	Pozivi.ucitajSale();
	Pozivi.ucitajOsobeUSelect();
};

