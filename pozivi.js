let Pozivi = (function () {
	var ajax = new XMLHttpRequest();
	var ucitaneSlike = []; 
	var last;
	var startIndex, endIndex; // indeks prve i zadnje trenutno prikazane slike (u nizu ucitaneSlike)
	

	function ucitajPodatkeImpl (funkcijaUcitavanje) {	
		ajax.onreadystatechange = function () {		
		    if (ajax.readyState == 4 && ajax.status == 200) {
		    	var objekat = JSON.parse(ajax.response);
		    	var periodicna = objekat["periodicna"];
		    	var vanredna = objekat["vanredna"];
		    	funkcijaUcitavanje(periodicna, vanredna);
		    }
		}

		ajax.open("GET", "http://localhost:8080/zauzeca", true);
		ajax.setRequestHeader("Content-Type", "application/json");
		ajax.send();
	}

	function uputiPostZahtjevImpl (dan, periodicna, mjesec, sala, pocetak, kraj, predavac, funkcijaBojenje) {
		ajax.onreadystatechange = function () {
			if (ajax.readyState == 4 && ajax.status == 200) {
				if (ajax.response.includes("Greška!")) {
					Swal.fire({
					  icon: 'error',
					  title: 'Greška',
					  text: ajax.response					  
					})
				} else {
			    	var objekatOdg = JSON.parse(ajax.response);
			    	var periodicnaNova = objekatOdg["periodicna"];
			    	var vanrednaNova = objekatOdg["vanredna"];
					funkcijaBojenje(periodicnaNova, vanrednaNova); // ucitava nova zauzeca i boji zauzeća
				}
			}

		};
		ajax.open("POST", "http://localhost:8080/rezervisi", true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.send("dan=" + dan + "&periodicna=" + periodicna + "&mjesec=" + mjesec + "&sala=" + sala + "&pocetak=" + pocetak + "&kraj=" + kraj + "&predavac=" + predavac);
	}

	function ucitajTriSlikeImpl () { 
		var pocetna, krajnja;
		var div = document.getElementById("imgContainer");
		if (ucitaneSlike.length == 0) {
			pocetna = 0;
			krajnja = 3;
			this.startIndex = 0;
			this.endIndex = 3;
		} else {
			if (this.endIndex - this.startIndex == 3) {
				this.endIndex += 3;
				this.startIndex +=3;
			} else if (this.endIndex - this.startIndex == 2) {
				this.endIndex += 2;
				this.startIndex += 3;
			} else if (this.endIndex - this.startIndex == 1) {
				this.endIndex += 1;
				this.startIndex += 3;
			} 
		}
		 // ako nije ucitana prije, posalji ajax zahtjev da ih dobavi
		var pomocna = this.endIndex -3;
		if (ucitaneSlike.length <= pomocna) { 
			if (ucitaneSlike.length != 0) {
				pocetna = ucitaneSlike.length;
				krajnja = pocetna + last;
			}
			ajax.open("GET", "http://localhost:8080/slike?pocetna=" + pocetna + "&krajnja=" + krajnja, true);
			pocetna = encodeURIComponent(pocetna);
			krajnja = encodeURIComponent(krajnja);
			ajax.send();
		} else { // ucitaj iz niza ucitaneSlike
			removeOldImages(div);
			for (var i = this.startIndex; i < this.endIndex; i++) {
				if (ucitaneSlike[i] != undefined) {
					var img = document.createElement("img");
					img.src = ucitaneSlike[i];
					div.appendChild(img);
				} else document.getElementById("sljedeciBtn").disabled = true; 
			}
		}
		
		if (this.startIndex != 0) document.getElementById("prethodniBtn").disabled = false;	
		else { // ucitavanje prvih slika
			document.getElementById("prethodniBtn").disabled = true;
			document.getElementById("sljedeciBtn").disabled = false;
		}

		ajax.onreadystatechange = function() {
			if (ajax.readyState == 4 && ajax.status == 200) { 		
				if (ajax.response != "[]") { // kada su prikazane zadnje tri slike, ali smo pritisnuli dugme sljedeci, server ce vratiti odgovor "[]"
					var array = ajax.response.split(",");
					if (array.length < 3) document.getElementById("sljedeciBtn").disabled = true; // ako vrati manje od 3 slike, nema ih vise na serveru
					else document.getElementById("sljedeciBtn").disabled = false;
					// ispod se brisu odredjeni znakovi iz array-a jer ga formira u obliku npr. [""etf.png"", ""sarajevo.jpg""] - a nama treba samo dio unutar navodnika da predstavlja clan niza
					array[0] = array[0].slice(0, 0) + array[0].slice(1, array[0].length); // brišemo prvi znak prvog člana tj [ 
					var lastIndex = array.length-1;
					last = lastIndex + 1;
					Pozivi.endIndex = Pozivi.startIndex + last;
					array[lastIndex] = array[lastIndex].slice(0, array[lastIndex].length-1) + array[lastIndex].slice(array[lastIndex].length, array[lastIndex].length); // brišemo zadnji znak u nizu tj ]
					removeOldImages(div);
					for (var i = 0; i < last; i++) {
						var source = array[i].toString().replace(/"/, ""); // obrišemo prvi znak "
						source = source.slice(0, source.length-1) + source.slice(source.length, source.length); // obrišemo zadnji znak "
						var img = document.createElement("img");
						img.src = source;
						div.appendChild(img);
						ucitaneSlike.push(source);
					}
				} else document.getElementById("sljedeciBtn").disabled = true; // ovim oznacavamo da nema vise slika na serveru
				
			}
		};	

	}

	function prikaziPrethodneSlikeImpl() {
		// ova funkcija ce se pozvati tek kada nismo prvi put na stranici, jer je u suprotnom dugme prethodni disabled
		document.getElementById("sljedeciBtn").disabled = false; // cim smo pritisnuli dugme prethodni, mozemo se vratiti naprijed na sljedeci
		// prvo brisemo trenutno prikazane slike
		var div = document.getElementById("imgContainer");
		removeOldImages(div);
		// racunamo indekse unutar niza ucitanih slika kojima trebamo pristupati sada
		if (this.endIndex - this.startIndex == 3) {
			this.endIndex -= 3;
			this.startIndex -=3;
		} else if (this.endIndex - this.startIndex == 2) {
			this.endIndex -= 2;
			this.startIndex -= 3;
		} else if (this.endIndex - this.startIndex == 1) {
			this.endIndex -= 1;
			this.startIndex -= 3;
		} 
		if (this.startIndex == 0) document.getElementById("prethodniBtn").disabled = true;
		else document.getElementById("prethodniBtn").disabled = false;
		//prikazujemo slike
		for (var i = this.startIndex; i < this.endIndex; i++) {
			var img = document.createElement("img");
			img.src = ucitaneSlike[i];
			div.appendChild(img);
		}
	}

	function ucitajOsobeImpl () {
		ajax.onreadystatechange = function () {		
			if (ajax.readyState == 4 && ajax.status == 200) {
			    var tabela = document.getElementById("tabelaOsobe");
			    var tbody = document.getElementsByTagName("tbody")[0];
			    var i;
			    var jsonOsobe = JSON.parse(ajax.response); 
			    var id = 1;
			    var ajax2 = new XMLHttpRequest();
				ajax2.open("GET", "http://localhost:8080/termini/" + id, true);
				ajax2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				ajax2.send();
				ajax2.onreadystatechange = function () {
						if (ajax2.readyState == 4 && ajax2.status == 200) {							
							
							var red = document.createElement("tr");	
							red.classList.add("red");				
							var kolona = document.createElement("td");		
							var idManji = id-1;			
							var tekst = document.createTextNode(jsonOsobe[idManji]["ime"] + " " + jsonOsobe[idManji]["prezime"]);
							kolona.appendChild(tekst);
							var kolona2 = document.createElement("td");
							var tekst2 = document.createTextNode(String(ajax2.response));
							kolona2.appendChild(tekst2);
							red.appendChild(kolona);
							red.appendChild(kolona2);
							tbody.appendChild(red);
							id++;
							if (id > jsonOsobe.length) return;
							ajax2.open("GET", "http://localhost:8080/termini/" + id, true);
							ajax2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
							ajax2.send();
							

						}
				};

			    setInterval(function() {
			    	id = 1;
			    	var ajax2 = new XMLHttpRequest();
					ajax2.open("GET", "http://localhost:8080/termini/" + id, true);
					ajax2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					ajax2.send();
					ajax2.onreadystatechange = function () {
						if (ajax2.readyState == 4 && ajax2.status == 200) {
							var red = document.getElementsByTagName("tr")[id];
							var kolona = red.getElementsByTagName("td")[1];
							console.log(kolona.firstChild.nodeValue);
							if (String(ajax2.response) != kolona.firstChild.nodeValue) {
								kolona.removeChild(kolona.firstChild);
								var noviTekst = document.createTextNode(String(ajax2.response));
								kolona.appendChild(noviTekst);
							}
							id++;
							if (id > jsonOsobe.length) return;
							ajax2.open("GET", "http://localhost:8080/termini/" + id, true);
							ajax2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
							ajax2.send();
						}
					};
				}, 30000);
				

			}
		}

		ajax.open("GET", "http://localhost:8080/osoblje", true);
		ajax.setRequestHeader("Content-Type", "application/json");
		ajax.send();
	}

	function ucitajSaleImpl() {
		var ajax = new XMLHttpRequest();
		ajax.onreadystatechange = function () {		
			if (ajax.readyState == 4 && ajax.status == 200) {
			    var select = document.getElementById("sala");
			    var i;
			    var jsonSale = JSON.parse(ajax.response);
			    for (i = 0; i < jsonSale.length; i++) {
				    var opcija = document.createElement("option");
				    opcija.classList.add("opcija");
				    var tekst = document.createTextNode(jsonSale[i].naziv);
					opcija.appendChild(tekst);
					select.appendChild(opcija);
				}
			}
		}

		ajax.open("GET", "http://localhost:8080/sale", true);
		ajax.setRequestHeader("Content-Type", "application/json");
		ajax.send();
	}

	function ucitajOsobeUSelectImpl() {
		var ajax = new XMLHttpRequest();
		ajax.onreadystatechange = function () {		
			if (ajax.readyState == 4 && ajax.status == 200) {
			    var select = document.getElementById("osobeSelect");
			    var i;
			    var jsonOsobe = JSON.parse(ajax.response);
			    for (i = 0; i < jsonOsobe.length; i++) {
				    var opcija = document.createElement("option");
				    var tekst = document.createTextNode(jsonOsobe[i].ime + " " + jsonOsobe[i].prezime);
					opcija.appendChild(tekst);
					select.appendChild(opcija);
				}
			}
		}

		ajax.open("GET", "http://localhost:8080/osoblje", true);
		ajax.setRequestHeader("Content-Type", "application/json");
		ajax.send();
	}

	return {
		ucitajPodatke : ucitajPodatkeImpl,
		uputiPostZahtjev : uputiPostZahtjevImpl,
		ucitajTriSlike : ucitajTriSlikeImpl,
		prikaziPrethodneSlike : prikaziPrethodneSlikeImpl,
		ucitajOsobe : ucitajOsobeImpl,
		ucitajSale : ucitajSaleImpl,
		ucitajOsobeUSelect : ucitajOsobeUSelectImpl
	}
}());

function ucitaj() {
	Pozivi.ucitajPodatke();
}

function removeOldImages(div) {
	while(div.firstChild) {
		div.removeChild(div.firstChild);
	}
}

function sljedece () {
	Pozivi.ucitajTriSlike();
}

function prethodne () {
	Pozivi.prikaziPrethodneSlike();
	
}