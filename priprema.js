const db = require('./db.js')
db.sequelize.sync({force:true}).then(function(){
    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
});
function inicializacija(){
	var osobljeListaPromisea = [];
	var rezervacijaListaPromisea = [];
	var terminListaPromisea = [];
	var salaListaPromisea = [];

	return new Promise(function(resolve,reject){
		osobljeListaPromisea.push(db.osoblje.create({id: 1, ime:'Neko', prezime: 'Nekić', uloga: 'profesor'}));
		osobljeListaPromisea.push(db.osoblje.create({id: 2, ime:'Drugi', prezime: 'Neko', uloga: 'asistent'}));
		osobljeListaPromisea.push(db.osoblje.create({id: 3, ime:'Test', prezime: 'Test', uloga: 'asistent'}));
		Promise.all(osobljeListaPromisea).then(function(osoblje){		

			salaListaPromisea.push(db.sala.create({id: 1, naziv:'1-11', zaduzenaOsoba: 1}));
	        salaListaPromisea.push(db.sala.create({id: 2, naziv:'1-15', zaduzenaOsoba: 2}));

	        Promise.all(salaListaPromisea).then(function(sale){
	        	var sala1 = sale.filter(function(s){return s.id===1})[0];

	        	terminListaPromisea.push(db.termin.create({id: 1, redovni: false, datum: '01.01.2020', pocetak: '12:00', kraj: '13:00'})); // unos time mozda nije dobar
	        	terminListaPromisea.push(db.termin.create({id: 2, redovni: true, dan: 0, semestar: 'zimski', pocetak: '13:00', kraj: '14:00'}));

	        		Promise.all(terminListaPromisea).then(function(termini) {

	        			rezervacijaListaPromisea.push(db.rezervacija.create({termin: 1, sala: 1, osoba: 1}).then(function(r){
							return new Promise(function(resolve,reject){resolve(r);});
						}));
						rezervacijaListaPromisea.push(db.rezervacija.create({termin: 2, sala: 1, osoba: 3}).then(function(r){
							return new Promise(function(resolve,reject){resolve(r);});
						}));
						Promise.all(rezervacijaListaPromisea).then(function(r){
							// dodjeljujemo osobama odgovarajuce rezervacije
							var osoba1 = osoblje.filter(function(o){return o.id===1})[0];
							osoba1.setRezervacije(r[0]);
							var osoba3 = osoblje.filter(function(o){return o.id===3})[0];
							osoba3.setRezervacije(r[1]);
							// dodjeljujemo salama odgovarajuce rezervacije
							var nizR = [];
							nizR.push(r[0]);
							nizR.push(r[1]);
							sala1.setRezervacije(nizR); // obje rezervacije su u sali 1-11

						}).catch(function(err){console.log("Rezervacija greska "+err);});
	        		}).catch(function(err){console.log("Termini greska "+err);});  
	        }).catch(function(err){console.log("Sale greska "+err);});  
		}).catch(function(err){console.log("Osoblje greska "+err);});  
	});

}