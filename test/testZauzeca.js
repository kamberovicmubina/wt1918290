const supertest = require("supertest");
const assert = require('assert');
const app = require("../index");


describe('BITNO: Da bi testovi prošli, potrebno je obrisati sve iz baze ukoliko su unešena nova zauzeća, pozvati iz konzole node priprema.js, pa tek onda pokrenuti testove!', function() {
	 describe('Ruta za dohvatanje osoblja', function() {
	 	it('Dohvatanje inicijalih osoba iz baze', function(done) {
	 		var svoOsoblje = [{"id" : 1, "ime" : "Neko", "prezime" : "Nekić", "uloga" : "profesor"}, {"id" : 2, "ime" : "Drugi", "prezime" : "Neko", "uloga" : "asistent"},{"id" : 3, "ime" : "Test", "prezime" : "Test", "uloga" : "asistent"}];
	 		supertest(app)
	 			.get("/osoblje")
	 			.expect(svoOsoblje)
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			})
	 	});

	 });

	 describe('Ruta za dohvatanje sala', function() {
	 	it('Dohvatanje sala iz baze', function(done) {
	 		var sveSale = [{"naziv" : "1-11", "zaduzenaOsoba" : 1}, {"naziv" : "1-15", "zaduzenaOsoba" : 2}];
	 		supertest(app)
	 			.get("/sale")
	 			.expect(sveSale)
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			})
	 	});

	 });

	 describe('Ruta za dohvatanje zauzeća', function() { 
	 	it('Dohvatanje inicijalnih zauzeća', function(done) {
	 		var pocetnaZauzeca = {"periodicna" : [{"dan" : 0, "semestar" : "zimski" , "naziv" : "1-11", "pocetak" : "13:00", "kraj" : "14:00", "predavac" : "Test Test"}], 
	 			"vanredna" : [{"datum" : "01.01.2020", "naziv" : "1-11", "pocetak" : "12:00", "kraj" : "13:00", "predavac": "Neko Nekić"}]};
	 		supertest(app)
		      .get("/zauzeca")
		      .expect(pocetnaZauzeca)
		      .end(function(err, res){
		        if (err) done(err);
		        done();
		      });
	 	});
	 	let objekatZauzeca = {"periodicna" : [{"dan" : 0, "semestar" : "zimski" , "naziv" : "1-11", "pocetak" : "13:00", "kraj" : "14:00", "predavac" : "Test Test"},
	 			{"dan" : 4, "semestar" : "zimski", "naziv" : "1-11", "pocetak" : "09:00", "kraj" : "10:00", "predavac" : "Drugi Neko"}], 
	 			"vanredna" : [{"datum" : "01.01.2020", "naziv" : "1-11", "pocetak" : "12:00", "kraj" : "13:00", "predavac": "Neko Nekić"}]};

	 	it('Kreiranje i vraćanje nove rezervacije', function (done) {

	 		supertest(app)
	 			.post("/rezervisi")
	 			.send({"dan" : 3, "periodicna" : true, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "09:00", "kraj" : "10:00", "predavac" : "Drugi Neko"})
	 			.expect(objekatZauzeca)
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da druga osoba kreira periodičnu rezervaciju na dan zauzet drugom periodičnom rezervacijom', function(done) {
	 		//Pokušat ćemo kreirati rezervaciju koja se preklapa sa periodičnom rezervacijom ponedjeljkom 13:00-14:00 u 1-11 tokom zimskog semestra
	 		var novaRezervacija = {"dan" : 6, "periodicna" : true, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "13:20", "kraj" : "15:00", "predavac" : "Drugi Neko"};
	 		
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Test Test.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da ista osoba kreira vanrednu rezervaciju koja se preklapa s postojećom periodičnom', function(done){
	 		//Pokušat ćemo kreirati rezervaciju koja se preklapa sa periodičnom rezervacijom ponedjeljkom 13:00-14:00 u 1-11 tokom zimskog semestra
	 		var novaRezervacija = {"dan" : 6, "periodicna" : true, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "13:20", "kraj" : "15:00", "predavac" : "Test Test"};
	 		
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Test Test.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da druga osoba kreira vanrednu rezervaciju na dan zauzet drugom periodičnom rezervacijom', function(done) {
	 		//Pokušat ćemo kreirati rezervaciju koja se preklapa sa periodičnom rezervacijom ponedjeljkom 13:00-14:00 u 1-11 tokom zimskog semestra
	 		var novaRezervacija = {"dan" : 6, "periodicna" : false, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "13:20", "kraj" : "15:00", "predavac" : "Drugi Neko"};
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Test Test.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da ista osoba kreira vanrednu rezervaciju koja se preklapa s postojećom periodičnom', function(done){
			//Pokušat ćemo kreirati rezervaciju koja se preklapa sa periodičnom rezervacijom ponedjeljkom 13:00-14:00 u 1-11 tokom zimskog semestra
	 		var novaRezervacija = {"dan" : 6, "periodicna" : false, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "13:20", "kraj" : "15:00", "predavac" : "Test Test"};
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Test Test.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da druga osoba kreira periodičnu rezervaciju na dan zauzet drugom vanrednom rezervacijom', function(done) {
	 		//Pokušat ćemo kreirati rezervaciju koja se preklapa sa vanrednom rezervacijom 01.01.2020 u 1-11 od 12:00 do 13:00
	 		var novaRezervacija = {"dan" : 1, "periodicna" : true, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "12:00", "kraj" : "14:00", "predavac" : "Drugi Neko"};
	 		
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Neko Nekić.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da ista osoba kreira novu periodičnu rezervaciju koja se preklapa s postojećom vanrednom', function(done) {
			//Pokušat ćemo kreirati rezervaciju koja se preklapa sa vanrednom rezervacijom 01.01.2020 u 1-11 od 12:00 do 13:00
	 		var novaRezervacija = {"dan" : 1, "periodicna" : true, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "12:00", "kraj" : "14:00", "predavac" : "Neko Nekić"};
	 		
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Neko Nekić.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da druga osoba kreira vanrednu rezervaciju na dan zauzet drugom vanrednom rezervacijom', function(done) {
	 		//Pokušat ćemo kreirati rezervaciju koja se preklapa sa vanrednom rezervacijom 01.01.2020 u 1-11 od 12:00 do 13:00
	 		var novaRezervacija = {"dan" : 1, "periodicna" : false, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "11:00", "kraj" : "13:30", "predavac" : "Drugi Neko"};
	 		
	 		// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Neko Nekić.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});
	 	});

	 	it('Pokušaj da ista osoba kreira novu vanrednu rezervaciju koja se preklapa s postojećom vanrednom', function(done) {
	 		var novaRezervacija = {"dan" : 1, "periodicna" : false, "mjesec" : "Januar", "sala" : "1-11", "pocetak" : "11:00", "kraj" : "13:30", "predavac" : "Neko Nekić"};
			// treba vratiti poruku ko je zauzeo termin
	 		supertest(app)
	 			.post("/rezervisi")
	 			.send(novaRezervacija)
	 			.expect("Greška! Termin je već zauzeo/la Neko Nekić.")
	 			.end(function(err,res) {
	 			});

	 		supertest(app)
	 			.get("/zauzeca")
	 			.expect(objekatZauzeca) // ocekujemo da su ostale iste rezervacije, nema ove nove
	 			.end(function(err, res) {
	 				if(err) done(err);
	 				done();
	 			});

	 	});

	 	

	 });

});